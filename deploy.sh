#!/bin/sh

dbpassword=

function get_dbpassword() {
    if [ -z "$dbpassword" ]; then
        read -sp "Enter Database password: " dbpassword;
    fi
}

function red () {
    echo -e "\e[0;91m${1}\e[0m";
}

function green () {
    echo -e "\e[0;92m${1}\e[0m";
}

if [[ $(id -u) -ne 0 ]]; then
    echo $(red "This script must be run as root")
    exit 1;
fi

function install_php () {
    # Add Remi rep
    dnf install -y dnf-utils http://rpms.remirepo.net/enterprise/remi-release-8.rpm

    # Install php 8
    dnf module reset php -y
    dnf module enable php:remi-8.0 -y
    dnf install php php-cli php-common php-fpm -y

    # install neccessary extensions
    dnf install -y php80-php-bcmath php80-php-json php80-php-mbstring php80-php-mysqlnd php-mysql

    # make sure php-fpm is running as the nginx user
    sed -i -E 's/^user\s*=.*$/user = nginx/' /etc/php-fpm.d/www.conf
    sed -i -E 's/^group\s*=.*$/group = nginx/' /etc/php-fpm.d/www.conf

    systemctl enable php-fpm
    systemctl start php-fpm
}

function install_nginx () {
    dnf install -y nginx
    systemctl enable nginx
    systemctl start nginx
    firewall-cmd --zone=public --permanent --add-service=http --add-service=https
    firewall-cmd --reload
}

function install_certbot () {
    dnf install -y snapd
    systemctl restart snapd.seeded.service
    systemctl enable --now snapd.socket
    [ ! -d "/snap"] && ln -s /var/lib/snapd/snap /snap

    snap install core
    snap refresh core

    snap install --classic certbot
    [ ! -f /usr/bin/certbot ] && ln -s /snap/bin/certbot /usr/bin/certbot
}

function configure_nginx () {
    echo -e "Please make sure your DNS is pointing to the following address\n"
    ip addr show $2 | grep inet | awk '{print $2}' | sed 's/\/.*$//'
    echo -e "Type \"continue\" to keep going\n"
    read REPLY;

    echo "$REPLY";
    if [ "$REPLY" != "continue" ]; then
        red "User Aborted"
        exit 1;
    fi

    if [ -z "$1" ]; then
        red "Please enter your domain name as the second argument"
        exit 1;
    fi

    cat <<EOF > "/etc/nginx/conf.d/$1.conf"
# Server block. Can have multiple per file
server {
	# port to listen on
	listen 80;
	# FQDN
	server_name $1;
	# Where to search for files
	root /var/www/$1/public;
	
	# Custom headers
	add_header X-Frame-Options "SAMEORIGIN";
	add_header X-Content-Type-Options "nosniff";
	
	# default file to load relative to root. serched in order;
	index index.php index.html;
	
	# Specific instruction for a location
	location / {
		# Try the folliwing in order - uri as a file,
		# uri as a directory, pass only the query string to index.php
	  try_files \$uri \$uri/ /index.php?\$query_string;
	}
	
	# Don't log requests for favicon and robots.txt
	location = /favicon.ico { access_log off; log_not_found off; }
	location = /robots.txt { access_log off; log_not_found off; }
	
	# proxy files ending in php to the fastcgi parser
	location ~ \.php {
		fastcgi_pass unix:/var/run/php-fpm/www.sock;
		# set parameter sfor the php fascg-git socket
		fastcgi_param SCRIPT_FILENAME \$realpath_root\$fastcgi_script_name;
		# incldue the conf for default CGI parameters
		include fastcgi_params;
	}
	
	# Deny access to dot files (exect .well-known)
	location ~ /\.(?|well-known).* {
		deny all;
	}
}
EOF

    install_certbot

    certbot --nginx --domain $1

    certbot renew --dry-run

    # allow nginx to modify storage
    chcon -t httpd_sys_rw_content_t "/var/www/$1/storage" -R

    systemctl restart php-fpm
    systemctl reload nginx
}

function install_mysql () {
    dnf install -y mysql-server
    systemctl enable mysqld.service
    systemctl start mysqld.service

    mysql_secure_installation

    get_dbpassword

    mysql -u root -p"$dbpassword" --execute="CREATE DATABASE $1;"

    mysql -u root -p"$dbpassword" --execute="CREATE USER 'laravel'@'localhost' IDENTIFIED BY '$dbpassword';"
    mysql -u root -p"$dbpassword" --execute="GRANT ALL PRIVILEGES ON $1.* TO 'laravel'@'localhost'; FLUSH PRIVILEGES;"
}

function install_composer () {
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php --install-dir=/usr/bin --filename=composer
    php -r "unlink('composer-setup.php');"
    composer self-update
}

function initialize_repo () {
    dnf install -y git

    user=$(who am i | awk '{print $1}')

    rm -rf "/var/opt/$1"
    mkdir -p  "/var/opt/$1"
    mkdir -p "/var/www/$1"

    chown -R nginx:nginx "/var/www/$1"
    chmod -R g+w "/var/www/$1"
    usermod "$user" -a -G nginx
    chown -R nginx:nginx "/var/opt/$1"

    su -c "cd /var/opt/$1; git init --bare " - "$user"
    cat <<EOF > "/var/opt/$1/hooks/post-receive"
function getBranchName()
{
    echo \$(git rev-parse --abbrev-ref HEAD)
}

currentBranch=\$(getBranchName)
if [[ \$currentBranch = "master" ]]; then
    git clone --git-dir=/var/opt/$1 --work-tree=/var/www/$1 checkout master

    composer install --optimize-autoloader --no-dev

    cd /var/www/$1

    php artisan migrate

    php artisan config:cache

    php artisan route:cache

    php artisan view:cache

    npm install
    npm run production
fi
EOF

    chmod a+x "/var/opt/$1/hooks/post-receive"
    chown -R nginx:nginx "/var/opt/$1"
}

function setup_laravel() {
    website="$1"
    database="$2"

    get_dbpassword

    cat <<EOF > "/var/www/$1/.env"
APP_NAME=$1
APP_ENV=local
APP_KEY=
APP_DEBUG=false
APP_URL=https://$1

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=$2
DB_USERNAME=root
DB_PASSWORD=$dbpassword
EOF

    chown -R nginx:nginx /var/www/$1/.env

    su -s /bin/bash nginx -c "cd /var/www/$1 && \
    composer install --optimize-autoloader --no-dev && \
    php artisan key:generate && \
    php artisan config:cache && \
    php artisan route:cache && \
    php artisan view:cache && \
    php artisan migrate && \
    php artisan passport:keys"
}



function all() {
    install_php
    install_nginx
    configure_nginx $1 $3
    install_mysql $2
    install_composer
    initialize_repo $1
    setup_laravel $1 $2
}

case $1 in
    ""|"all")
        all $2 $3 $4
        ;;

    "install_php")
        install_php
        ;;

    "install_nginx")
        install_nginx
        ;;

    "configure_nginx")
        configure_nginx $2 $3
        ;;

    "install_mysql")
        install_mysql $2
        ;;

    "initialize_repo")
        initialize_repo $2
        ;;
    
    "setup_laravel")
        setup_laravel $2 $3
        ;;
    
    *)
        red "Invalid Command"
        ;;

esac